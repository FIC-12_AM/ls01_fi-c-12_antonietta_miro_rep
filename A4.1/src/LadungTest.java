import java.util.Scanner;
public class LadungTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scn = new Scanner(System.in);
		
		Ladung a = new Ladung();
		Ladung b = new Ladung();
		Ladung c = new Ladung();
		Ladung d = new Ladung();
		Ladung e = new Ladung();
		Ladung f = new Ladung();
		Ladung g = new Ladung();
		
		
		a.setBezeichnung("Ferengi Schneckensaft");
		System.out.println(a.getBezeichnung());
		a.setMenge(200);
		System.out.println(a.getMenge());
		
		b.setBezeichnung("Batleth Klingonen Schwert");
		System.out.println(b.getBezeichnung());
		b.setMenge(200);
		System.out.println(b.getMenge());
		
		c.setBezeichnung("Borg-Schrott");
		System.out.println(c.getBezeichnung());
		c.setMenge(5);
		System.out.println(c.getMenge());
	
		d.setBezeichnung("Rote-Materie");
		System.out.println(d.getBezeichnung());
		d.setMenge(2);
		System.out.println(d.getMenge());
		
		e.setBezeichnung("Plasma-Waffe");
		System.out.println(e.getBezeichnung());
		e.setMenge(50);
		System.out.println(e.getMenge());
		
		f.setBezeichnung("Forschungssonde");
		System.out.println(f.getBezeichnung());
		f.setMenge(35);
		System.out.println(f.getMenge());
		
		g.setBezeichnung("Photonentorpedo");
		System.out.println(g.getBezeichnung());
		g.setMenge(3);
		System.out.println(g.getMenge());
	}

}
