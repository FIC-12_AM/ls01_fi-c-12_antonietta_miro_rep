import java.util.ArrayList;
import java.util.*;
public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebensErhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;	
	private ArrayList<Ladung> ladungsverzeichnis;

	
	
	public Raumschiff() {
		System.out.println("Raumschiff Anfangszustand:");
		this.photonentorpedoAnzahl=0;
		this.energieversorgungInProzent=0;
		this.schildeInProzent=0;
		this.huelleInProzent=0;
		this.lebensErhaltungssystemeInProzent=0;
		this.androidenAnzahl=0;
		this.schiffsname="unbekannt";
		this.ladungsverzeichnis = new ArrayList<Ladung>();
	}
	public Raumschiff(int photonentorpedoAnzahl,
					  int energieversorgungInProzent, 
			          int schildeInProzent, 
			          int huelleInProzent, 
			          int lebensErhaltungssystemeInProzent, 
			          int androidenAnzahl,
			          String schiffsname) {	
		this.photonentorpedoAnzahl=photonentorpedoAnzahl;
		this.energieversorgungInProzent=energieversorgungInProzent;
		this.schildeInProzent=schildeInProzent;
		this.huelleInProzent=huelleInProzent;
		this.lebensErhaltungssystemeInProzent=lebensErhaltungssystemeInProzent;
		this.androidenAnzahl=androidenAnzahl;
		this.schiffsname=schiffsname;
		this.ladungsverzeichnis = new ArrayList<Ladung>();
	}
	
	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}
	
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;	
	}
	 
	 int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	public int getLebensErhaltungssystemeInProzent() {
		return lebensErhaltungssystemeInProzent;
	}
	public void setLebensErhaltungssystemeInProzent(int lebensErhaltungssystemeInProzent) {
		this.lebensErhaltungssystemeInProzent = lebensErhaltungssystemeInProzent;
	}
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	public String getSchiffsname() {
		return schiffsname;
	}
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	public void addLadung(Ladung neueladung) {
		this.ladungsverzeichnis.add(neueladung);	
	}
	
	public void zustandRaumschiff() {
		System.out.println("\nSchiffsname: " + this.schiffsname);
		System.out.println("Photonentorpedo Anzahl: " + this.photonentorpedoAnzahl);
		System.out.println("Energieversorgung in %: " + this.energieversorgungInProzent);
		System.out.println("Schilde in %: " + this.schildeInProzent);
		System.out.println("H�lle in %: " + this.huelleInProzent);
		System.out.println("Lebenserhaltungssystem in %: " + this.lebensErhaltungssystemeInProzent);
		System.out.println("Androidenanzahl: " + this.androidenAnzahl);
		
		
	}
	
	public void zustandLadung() {
		System.out.println("\nLadungsverzeichnis: " + schiffsname + this.ladungsverzeichnis.toString());
		
	}
	
	public void photonentorpedosAbschiessen(Raumschiff ziel) {
		if (this.photonentorpedoAnzahl<=0) {
			this.photonentorpedoAnzahl = 0;
			nachrichtAnAlle("\nNachricht an alle: -=*Click*=- ");
	
		}
		else {
			this.photonentorpedoAnzahl = this.photonentorpedoAnzahl-1;
			nachrichtAnAlle("\nNachricht an alle: Photonentorpedo abgeschossen");
			treffer(ziel);
		}
		
	}
public void nachrichtAnAlle(String message) {
	System.out.println(message);
}
public void treffer(Raumschiff ziel) {
	System.out.println(ziel.schiffsname + " wurde getroffen");
}

public void phaserkanoneAbschiessen(Raumschiff ziel) {
	if  (this.energieversorgungInProzent<=50) {
		this.energieversorgungInProzent =50;
		nachrichtAnAlle("\nNachricht an alle: -=*Click*=- ");
		
	}
	else {
		this.energieversorgungInProzent = this.energieversorgungInProzent-50;
		nachrichtAnAlle("\nNachricht an alle: Phaserkanone abgeschossen");
		treffer(ziel);
	

	}
}
}

