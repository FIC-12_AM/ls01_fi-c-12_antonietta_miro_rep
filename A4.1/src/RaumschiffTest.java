import java.util.Scanner;

public class RaumschiffTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scn = new Scanner(System.in);
		
		Raumschiff klingonen = new Raumschiff();
		
		
		klingonen.setPhotonentorpedoAnzahl(1);
		//System.out.println(a.getPhotonentorpedoAnzahl());
		klingonen.setEnergieversorgungInProzent(100);
		//System.out.println(a.getEnergieversorgungInProzent());
		klingonen.setSchildeInProzent(100);
		//System.out.println(a.getSchildeInProzent());
		klingonen.setHuelleInProzent(100);
		//System.out.println(a.getHuelleInProzent());
		klingonen.setLebensErhaltungssystemeInProzent(100);
		//System.out.println(a.getLebensErhaltungssystemeInProzent());
		klingonen.setSchiffsname("IKS Hegh ta");
		//System.out.println(a.getSchiffsname());
		klingonen.setAndroidenAnzahl(2);
		//System.out.println(a.getAndroidenAnzahl());
		
		
		Ladung l1 = new Ladung("Ferengi Schneckensaft",200);
		klingonen.addLadung(l1);
		
		klingonen.zustandRaumschiff();
		Ladung l2 = new Ladung("Bat'leth Klingonen Schwert",200);
		klingonen.addLadung(l2);
		
		Raumschiff romulaner = new Raumschiff();
		romulaner.setPhotonentorpedoAnzahl(2);
		romulaner.setEnergieversorgungInProzent(100);
		romulaner.setSchildeInProzent(100);
		romulaner.setHuelleInProzent(100);
		romulaner.setLebensErhaltungssystemeInProzent(100);
		romulaner.setSchiffsname("IRW Khazara");
		romulaner.setAndroidenAnzahl(2);

	
		Ladung l3 = new Ladung("Borg Schrott",5);
		romulaner.addLadung(l3);
		romulaner.zustandRaumschiff();
		Ladung l4 = new Ladung("Rote Materie",2);
		romulaner.addLadung(l4);
		Ladung l5 = new Ladung("Plasma-Waffe",50);
		romulaner.addLadung(l5);
		
		Raumschiff vulkanier = new Raumschiff();
		vulkanier.setPhotonentorpedoAnzahl(0);
		vulkanier.setEnergieversorgungInProzent(80);
		vulkanier.setSchildeInProzent(80);
		vulkanier.setHuelleInProzent(50);
		vulkanier.setLebensErhaltungssystemeInProzent(100);
		vulkanier.setSchiffsname("NiVar");
		vulkanier.setAndroidenAnzahl(5);
		
		Ladung l6 = new Ladung ("Forschungssonde",35);
		Ladung l7 = new Ladung("Photonentorpedo",3);
		vulkanier.addLadung(l6);
		vulkanier.addLadung(l7);
		vulkanier.zustandRaumschiff();
		
		//System.out.println("Ladung Raumschiff IKS Hegh ta:");
		klingonen.zustandLadung();
		
		romulaner.zustandLadung();
		//System.out.println("Ladung Raumschiff IRW Khazara:");
		
		vulkanier.zustandLadung();
		//System.out.println("Ladung Raumschiff NiVar:");
		
System.out.println("\n\n\n\nRaumschiff Endzustand:");		
klingonen.photonentorpedosAbschiessen(romulaner);	
romulaner.phaserkanoneAbschiessen(klingonen);		
vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
klingonen.zustandRaumschiff();
klingonen.zustandLadung();
klingonen.photonentorpedosAbschiessen(romulaner);
klingonen.photonentorpedosAbschiessen(romulaner);
klingonen.zustandRaumschiff();
klingonen.zustandLadung();
romulaner.zustandRaumschiff();
romulaner.zustandLadung();
vulkanier.zustandRaumschiff();
vulkanier.zustandLadung();
	}

}
