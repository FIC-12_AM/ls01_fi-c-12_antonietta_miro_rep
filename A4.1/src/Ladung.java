
public class Ladung {
	private String bezeichnung;
	private int menge;

	public Ladung() {
		this.bezeichnung="";
		this.menge=0;
		
	}
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	public String getBezeichnung() {
		return this.bezeichnung;
	}

	public void setBezeichnung(String name) {
		this.bezeichnung = name;
	}

	public int getMenge() {
		return menge;
	}

	public void setMenge(int menge) {
		this.menge = menge;
	}
	
	public void werteLadung() {
		System.out.println("Ladungsbezeichnung: " + this.bezeichnung);
		System.out.println("Menge: " + this.menge);
	}
	@Override
	public String toString() {
		return "\nLadungsbezeichnung = " + bezeichnung + ", Menge =" + menge;
	}
	

}
