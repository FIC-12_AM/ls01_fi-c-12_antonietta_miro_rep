﻿import java.util.Scanner;
class Fahrkartenautomat {
	
	public static void main(String[] args) {
					while (true) {
						int ihreWahl;
						System.out.println("");

			            double zuZahlenderBetrag = 0.0;

			            do {
			                ihreWahl = fahrkartenbestellungErfassung();

			                if (ihreWahl > 3 || ihreWahl < 0) {

			                    System.out.println("Ihre Wahl " + ihreWahl);
			                    //System.out.println(">>falsche Eingabe<<");
			                }
			                else if(ihreWahl != 9)
			                {
			                    zuZahlenderBetrag = zuZahlenderBetrag + fahrkartenbestellungErfassen(ihreWahl);
			                }
			                
			                System.out.println("");
			                System.out.println("");
			                System.out.println("");

			            } while (ihreWahl != 9);

					
						double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
						fahrkartenAusgeben();
						rueckgeldAusgeben(rückgabebetrag);

						System.out.println("\nBitte Fahrscheine vor Fahrtantritt entwerten!\n"
								+ "Wir wünschen Ihnen eine gute Fahrt.");

					}
				}	
			        
				public static int fahrkartenbestellungErfassung() {
					Scanner tastatur = new Scanner(System.in);
					int ihreWahl;

					System.out.println("Wählen Sie ihre Fahrkarte für Berlin AB aus:");
					System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
					System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
					System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
					System.out.println("Bezahlen(9)");

					System.out.printf("Geben Sie das gewünschte Ticket ein: ");

					ihreWahl = tastatur.nextInt();

					return ihreWahl;
				}

				public static double fahrkartenbestellungErfassen(int fahrkartenart) {
					Scanner tastatur = new Scanner(System.in);

					double anzahlTickets;

					double ticketEinzelpreis = 0;

					if (fahrkartenart == 1) {
						System.out.println("Preis pro Tickets: 2,90€ ");
						ticketEinzelpreis = 2.90;
					} else if (fahrkartenart == 2) {
						System.out.println("Preis pro Ticket: 8,60€ ");
						ticketEinzelpreis = 8.60;
					} else if (fahrkartenart == 3) {
						System.out.println("Preis pro Ticket: 23,50€ ");
						ticketEinzelpreis = 23.50;
					}

					do {
						System.out.print("Wie viele Tickets möchten Sie kaufen?(Max. 10): ");
						anzahlTickets = tastatur.nextInt();

						if (anzahlTickets > 10 || anzahlTickets < 0) {

							System.out.println(" >> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
						}

					} while (anzahlTickets > 10 || anzahlTickets < 0);

					return ticketEinzelpreis * anzahlTickets;

				}
				

				public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
					Scanner tastatur = new Scanner(System.in);

					double eingezahlterGesamtbetrag = 0.0;
					double eingeworfeneMünze;

					while (eingezahlterGesamtbetrag < zuZahlenderBetrag)

					{
						System.out.printf("%s%.2f EURO %s", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),
								"  ");
						System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
						eingeworfeneMünze = tastatur.nextDouble();
						eingezahlterGesamtbetrag += eingeworfeneMünze;
					}

					return eingezahlterGesamtbetrag - zuZahlenderBetrag;
				}

				public static void fahrkartenAusgeben() {
					System.out.println("\nFahrscheine werden ausgegeben.");
					for (int i = 0; i < 8; i++) {
						System.out.print("=");
						try {
							Thread.sleep(250);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					System.out.println("\n\n");
				}

				public static void rueckgeldAusgeben(double rückgabebetrag) {
					if (rückgabebetrag > 0.0) {
						System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
						System.out.println("wird in folgenden Münzen ausgezahlt:");

						while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
						{
							System.out.println("2 EURO");
							rückgabebetrag -= 2.0;
						}
						while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
						{
							System.out.println("1 EURO");
							rückgabebetrag -= 1.0;
						}
						while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
						{
							System.out.println("50 CENT");
							rückgabebetrag -= 0.5;
						}
						while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
						{
							System.out.println("20 CENT");
							rückgabebetrag -= 0.2;
						}
						while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
						{
							System.out.println("10 CENT");
							rückgabebetrag -= 0.1;
						}
						while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
						{
							System.out.println("5 CENT");
							rückgabebetrag -= 0.05;
						}
					}
					

			}
} 
//>>>>>>> branch 'master' of https://FIC-12_AM@bitbucket.org/FIC-12_AM/ls01_fi-c-12_antonietta_miro_rep.git
