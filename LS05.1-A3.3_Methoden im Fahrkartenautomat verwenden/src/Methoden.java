import java.util.Scanner;
public class Methoden {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);
	      
		double zuZahlenderBetrag = fahrkartenbestellungErfassen();
		double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(rückgabebetrag);

		System.out.println("\nVergessen Sie nicht, die Fahrscheine\n" + "vor Fahrtantritt entwerten zu lassen!\n"
		+ "Wir wünschen Ihnen eine gute Fahrt.");
		}

		public static double fahrkartenbestellungErfassen()  {
		Scanner tastatur = new Scanner(System.in);

		System.out.print("Preis pro Ticket (EURO): ");
		double ticketEinzelpreis = tastatur.nextDouble();

		System.out.print("Wie viele Tickets werden gekauft?(Max. 10): ");
		double anzahlTickets = tastatur.nextInt();

		if (anzahlTickets <= 10 && anzahlTickets > 0) {
		} else {

		System.out.println("Maximal 10 Tickets");

		System.exit(0);


		}
		return ticketEinzelpreis * anzahlTickets;

		}

		public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag)

		{
		System.out.printf("%s%.2f EURO %s", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),
		"  ");
		System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
		eingeworfeneMünze = tastatur.nextDouble();
		eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
		}

		public static void fahrkartenAusgeben() {
		System.out.println("\nFahrscheine werden ausgegeben.");
		for (int i = 0; i < 8; i++) {
		System.out.print("=");
		try {
		Thread.sleep(250);
		} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		}
		System.out.println("\n\n");
		}

		public static void rueckgeldAusgeben(double rückgabebetrag) {
		if (rückgabebetrag > 0.0) {
		System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
		System.out.println("wird in folgenden Münzen ausgezahlt:");

		while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
		{
		System.out.println("2 EURO");
		rückgabebetrag -= 2.0;
		}
		while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
		{
		System.out.println("1 EURO");
		rückgabebetrag -= 1.0;
		}
		while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
		{
		System.out.println("50 CENT");
		rückgabebetrag -= 0.5;
		}
		while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
		{
		System.out.println("20 CENT");
		rückgabebetrag -= 0.2;
		}
		while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
		{
		System.out.println("10 CENT");
		rückgabebetrag -= 0.1;
		}
		while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
		{
		System.out.println("5 CENT");
		rückgabebetrag -= 0.05;
		}
		}
		}
		}

